FROM registry.lendfoundry.com/base:beta8

ADD ./src/LendFoundry.EventHub.Abstractions /app/LendFoundry.EventHub.Abstractions
WORKDIR /app/LendFoundry.EventHub.Abstractions
RUN eval "$CMD_RESTORE"
RUN dnu build

ADD ./src/LendFoundry.EventHub.Client /app/LendFoundry.EventHub.Client
WORKDIR /app/LendFoundry.EventHub.Client
RUN eval "$CMD_RESTORE"
RUN dnu build

ADD ./src/LendFoundry.EventHub.Api /app/LendFoundry.EventHub.Api
WORKDIR /app/LendFoundry.EventHub.Api
RUN eval "$CMD_RESTORE"
RUN dnu build

EXPOSE 5000
ENTRYPOINT dnx kestrel
