﻿using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using Microsoft.AspNet.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.EventHub.Api.Controllers
{
    [Route("/")]
    public class ApiController : ExtendedController
    {
        public ApiController
        (
            ILogger logger,
            ITokenReader tokenReader,
            IEventHubClientFactory eventhubFactory
        ) : base(logger)
        {
            TokenReader = tokenReader;
            EventhubFactory = eventhubFactory;
        }

        private ITokenReader TokenReader { get; }

        private IEventHubClientFactory EventhubFactory { get; }

        [HttpPost("{eventName}/batch")]
        public Task<IActionResult> PublishBatch(string eventName, [FromBody] List<object> data)
        {
            return ExecuteAsync(async () =>
            {
                if (data == null)
                    return ErrorResult.BadRequest("Event data cannot be empty.");

                foreach (var item in data)
                    await Publish(eventName, item);

                return new HttpStatusCodeResult(204);
            });
        }

        [HttpPost("{eventName}")]
        public Task<IActionResult> Publish(string eventName, [FromBody]object data)
        {
            return ExecuteAsync(async () =>
            {
                await EventhubFactory
                    .Create(TokenReader)
                    .Publish(eventName, data);
                return new HttpStatusCodeResult(204);
            });
        }
    }
}
