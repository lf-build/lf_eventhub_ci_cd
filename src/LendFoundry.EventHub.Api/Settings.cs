﻿using LendFoundry.Foundation.Services.Settings;
using System;

namespace LendFoundry.EventHub.Api
{
    public class Settings
    {
        private const string Prefix = "EVENTHUB";

        public static string ServiceName { get; } = Prefix.ToLower();

        public static string Nats { get; } = Environment.GetEnvironmentVariable($"{Prefix}_NATS_URL") ?? "nats";

        public static ServiceSettings Tenant { get; } = new ServiceSettings($"{Prefix}_TENANT", "tenant");
    }
}