using LendFoundry.Security.Tokens;

namespace LendFoundry.EventHub.Client
{
    public interface IEventHubClientFactory
    {
        IEventHubClient Create(ITokenReader tokenReader);
    }
}