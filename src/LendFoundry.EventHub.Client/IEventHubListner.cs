﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.EventHub.Client
{
    public interface IEventHubListner
    {
        void On(string eventName, string tenant, Action<EventInfo> handler);
    }
}