﻿using LendFoundry.Foundation.Date;
using Newtonsoft.Json.Linq;
using System;

namespace LendFoundry.EventHub.Client
{
    [Serializable]
    public class EventInfo : IEventInfo
    {
        public string TenantId { get; set; }

        public string Id { get; set; }

        public string Name { get; set; }

        public TimeBucket Time { get; set; }

        public string Source { get; set; }

        public object Data { get; set; }

        public T Cast<T>() => JObject.FromObject(Data).ToObject<T>();

        public object Cast(Type type) => JObject.FromObject(Data).ToObject(type);
        public string IpAddress { get; set; }
        public string UserName { get; set; }
        public string Token { get; set; }
    }
}
