﻿#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif
using System;

namespace LendFoundry.EventHub.Client
{
    public class EventSubscriptionWrapper<TEvent> : IEventSubscriptionWrapper where TEvent : class
    {
        public EventSubscriptionWrapper(IServiceProvider provider)
        {
            Provider = provider;
        }

        public IServiceProvider Provider { get; set; }

        public string EventName => typeof (TEvent).Name;

        public void Handle(EventInfo @event)
        {
            Console.WriteLine("Handling event {0}", @event.Name);
            var subscription = Provider.GetService<IEventSubscription<TEvent>>();
            Console.WriteLine("Subscription for event {0} {1}", @event.Name, subscription != null ? "found" : "not found");
            subscription?.Handle(@event, @event.Cast<TEvent>());
            Console.WriteLine("Event {0} handled.", @event.Name);
        }
    }
}