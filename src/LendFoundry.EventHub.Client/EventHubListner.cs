﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using STAN.Client;
using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Diagnostics;


namespace LendFoundry.EventHub.Client
{
    public class EventHubListner : IEventHubListner
    {
        public EventHubListner
        (
            string natsServer,
            ILogger logger,
            string serviceName
        )
        {
            if (natsServer == null) throw new InvalidArgumentException("The Nats Server is required!", nameof(natsServer));
            if (string.IsNullOrEmpty(natsServer)) throw new InvalidArgumentException("The environment Variable is required!", nameof(natsServer));
            if (string.IsNullOrEmpty(serviceName)) throw new InvalidArgumentException("ServiceName is required!", nameof(serviceName));
            
            Logger = logger;
            ServiceName = serviceName;
            NatsServer = natsServer;
        }

        private string NatsServer { get; }

        private string ServiceName { get; set; }

        private ILogger Logger { get; }
        private ConcurrentDictionary<string, IStanSubscription> Handlers = new ConcurrentDictionary<string, IStanSubscription>();
        private ConcurrentDictionary<string, Action<EventInfo>> Subs = new ConcurrentDictionary<string, Action<EventInfo>>();

        /// <summary>
        /// Subscribe to given event by providing its name and a handler.
        /// </summary>
        /// <param name="eventName">The name of the event</param>
        /// <param name="handler">The handler delegate</param>
        public void On(string eventName, string tenant, Action<EventInfo> handler)
        {
            try
            {
                if (new[] { "/", "*" }.Contains(eventName))
                {
                    // When does not have event name informed
                    eventName = "all";
                }

                /* commenting this code as  now service name is compulsory
                // Generate a random channel if it's informed
                var isRandomChannel = string.IsNullOrEmpty(ServiceName);
                if (isRandomChannel)
                    ServiceName = Guid.NewGuid().ToString("N");
                    */

                ValidateEventName(eventName);

                if (!Handlers.ContainsKey(eventName))
                {
                    {
                        var options = StanSubscriptionOptions.GetDefaultOptions();
                        options.AckWait = 60000;
                        options.ManualAcks = true;                       
                        var messageTryCounter = new ConcurrentDictionary<string, int>();
                        Handlers.TryAdd(eventName, StanConnection.Subscribe(eventName, ServiceName, options, async (msg, args) =>
                        {

                            try
                            {
                                var eventInfo = JsonConvert.DeserializeObject<EventInfo>(Encoding.UTF8.GetString(args.Message.Data));
                                Action<EventInfo> thisHandler;
                                Subs.TryGetValue($"#{eventName}-#{eventInfo.TenantId}", out thisHandler);
                                if (thisHandler != null)
                                {
                                    StringBuilder sb = new StringBuilder();
                                    sb.AppendLine(DateTime.Now.ToString());
                                    sb.AppendLine("");
                                    sb.AppendLine("---------------- SUB -----------------");
                                    sb.AppendLine($"[Id         ]: {eventInfo.Id}");
                                    sb.AppendLine($"[time       ]: {eventInfo.Time.Time}  ");
                                    sb.AppendLine($"[event name ]: {eventInfo.Name}       ");
                                    sb.AppendLine($"[tenant     ]: {eventInfo.TenantId}   ");
                                    sb.AppendLine($"[event data ]: {eventInfo.Data}       ");
                                    sb.AppendLine($"[Redelivered]: {args.Message.Redelivered}");
                                    sb.AppendLine("-------------------------------------");
                                    sb.AppendLine("");
                                    Logger.Debug(sb.ToString());

                                    try
                                    {
                                        await Task.Run(() => thisHandler(eventInfo)).TimeoutAfter(60000);
                                        args.Message.Ack();
                                    }
                                    catch (Exception ex)
                                    {
                                        Logger.Error(ex.Message, ex);
                                        var tryCount = messageTryCounter.AddOrUpdate(eventInfo.Id, 1, (key, value) => value + 1);
                                        //If message is already tried twice, send the ACK and remove from Dictionary and throw exception
                                        if (tryCount >= 2)
                                        {
                                            args.Message.Ack();
                                           // messageTryCounter.TryRemove(eventInfo.Id, out tryCount);
                                            throw new Exception($"Maximum number of re-delivery attempted reached for {eventName} # {eventInfo.Id} ");
                                        }
                                        //If message is not processed in 60 seconds, throw exception so in this case ACK will not be sent and it will be Redelivered
                                        throw new Exception($"Message processing timedout for {eventName} # {eventInfo.Id}");
                                    }
                                }
                                else
                                {
                                    StringBuilder sb = new StringBuilder();
                                    sb.AppendLine(DateTime.Now.ToString());
                                    sb.AppendLine("");
                                    sb.AppendLine("No handler for: " + eventInfo.TenantId);
                                    sb.AppendLine("---------------- SUB -----------------");
                                    sb.AppendLine($"[Id         ]: {eventInfo.Id}");
                                    sb.AppendLine($"[time       ]: {eventInfo.Time.Time}  ");
                                    sb.AppendLine($"[event name ]: {eventInfo.Name}       ");
                                    sb.AppendLine($"[tenant     ]: {eventInfo.TenantId}   ");
                                    sb.AppendLine($"[event data ]: {eventInfo.Data}       ");
                                    sb.AppendLine($"[Redelivered]: {args.Message.Redelivered}");
                                    sb.AppendLine("-------------------------------------");
                                    sb.AppendLine("");
                                    Logger.Debug(sb.ToString());
                                }
                            }
                            catch (Exception ex)
                            {
                                Logger.Error($"Message from event #{eventName} could not be parsed: \n", ex);
                            }
                        }));
                    }
                }

                Subs.GetOrAdd($"#{eventName}-#{tenant}", handler);
            }
            catch (Exception ex)
            {
                Logger.Error($"Error while creating subscription to event #{eventName} :\n", ex);
                throw ex;
            }
        }

        private static readonly object Lock = new object();
        private IStanConnection lStanConnection = null;
        private IStanConnection StanConnection
        {
            get
            {
                if (lStanConnection == null)
                {
                    lock (Lock)
                    {
                        var options = StanOptions.GetDefaultOptions();
                        options.NatsURL = NatsServer;
                        var connFactory = new StanConnectionFactory();
                        lStanConnection = connFactory.CreateConnection("test-cluster", Guid.NewGuid().ToString("N"), options);
                    }
                }
                return lStanConnection;
            }
        }

        private static void ValidateEventName(string eventName)
        {
            // Verify all entry for queue handling.
            // Notes:
            // Unless stated otherwise, all binary sizes/integers on the wire are network byte order(ie.big endian)
            // Valid topic and channel names are characters[.a - zA - Z0 - 9_ -] and 1 < length <= 64 (max length was 32 prior to nsqd 0.2.28)

            if (string.IsNullOrEmpty(eventName))
                throw new ArgumentException("Event name must be informed to performs this operation.");

            if (eventName.Length > 64)
                throw new ArgumentException("Max length event name exceeded [64 characters].");

            if (Regex.Matches(eventName, "[.a-zA-Z0-9_-]", RegexOptions.IgnoreCase).Count != eventName.Length)
                throw new ArgumentException("Invalid characters found in event name, please review it [.a-zA-Z0-9_-].");
        }
    }
}

