﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using Newtonsoft.Json;
using RestSharp;
using STAN.Client;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using RestSharp.Serializers;
using Newtonsoft.Json.Converters;
using System.IO;
#if DOTNET2
using Microsoft.AspNetCore.Http;
#else
using Microsoft.AspNet.Http;
#endif

namespace LendFoundry.EventHub.Client
{
    public class EventHubClient : IEventHubClient
    {
        public EventHubClient
        (
            IServiceClient client,
            ITokenReader tokenReader,
            IEventHubListner eventHubListner,
            IHttpContextAccessor httpAccessor,
            ILogger logger,
            string serviceName
        )
        {
            if (tokenReader == null) throw new InvalidArgumentException("The TokenReader was not found!", nameof(tokenReader));
            if (eventHubListner == null) throw new InvalidArgumentException("The eventHubListner is required!", nameof(eventHubListner));
            if(string.IsNullOrEmpty(serviceName)) throw new InvalidArgumentException("ServiceName is required!");

            EventListner = eventHubListner;
            Client = client;
            TokenReader = tokenReader;
            HttpAccessor = httpAccessor;
            Logger = logger;
            ServiceName = serviceName;
           
        }

        private IServiceClient Client { get; }

        private string NatsServer { get; }

        private string ServiceName { get; set; }      

        private ITokenReader TokenReader { get; }

        private IHttpContextAccessor HttpAccessor { get; }
        private IEventHubListner EventListner { get; }

        private ILogger Logger { get; }

        /// <summary>
        /// Publish given event to Event Hub.
        /// </summary>
        /// <typeparam name="T">Type of the event.</typeparam>
        /// <param name="eventName">Name of event being published</param>
        /// <param name="event">Event data to be sent</param>
        /// <returns></returns>
        public async Task<bool> Publish<T>(string eventName, T @event)
        {
            return await Task.Run(() =>
            {
                var request = new RestRequest("/{eventName}", Method.POST)
                {
                    JsonSerializer = new JsonNetSerializer()
                };
                request.AddUrlSegment(nameof(eventName), eventName);
                request.AddJsonBody(@event);
                return Client.Execute(request);
            });
        }

        public async Task<bool> Publish<T>(T @event)
        {
            return await Publish(typeof(T).Name, @event);
        }

        public async void PublishBatchWithInterval<T>(List<T> events, int interval = 1)
        {
            if (interval > 1000)
                interval = 1000;

            foreach (var myEvent in events)
            {
                await Publish(myEvent.GetType().Name, myEvent);
                Thread.Sleep(interval);
            }
        }

        /// <summary>
        /// Subscribe to given event by providing its name and a handler.
        /// </summary>
        /// <param name="eventName">The name of the event</param>
        /// <param name="handler">The handler delegate</param>
        public void On(string eventName, Action<EventInfo> handler)
        {
            try
            {
                var tenant = GetToken(TokenReader).Tenant;
                EventListner.On(eventName, tenant, handler);
            }
            catch (Exception ex)
            {
                Logger.Error($"Error while creating subscription to event #{eventName} :\n", ex);
                throw ex;
            }
        }


        /// <summary>
        /// Starts to listen to all events subscribed.
        /// </summary>
        public void Start()
        {
            //IStanConnection conn = NatsConnect();
           // Parallel.ForEach(Subscribers, subscriber => subscriber.Connect(conn));
        }

        public async void StartAsync()
        {
            await Task.Run(() => Start());
        }

        private IToken GetToken(ITokenReader token)
        {
            return new TokenHandler(null, null, null).Parse(token.Read());
        }      

        private class JsonNetSerializer : ISerializer
        {
            private readonly Newtonsoft.Json.JsonSerializer serializer;

            public JsonNetSerializer()
            {
                ContentType = "application/json";
                serializer = new Newtonsoft.Json.JsonSerializer();
                serializer.MissingMemberHandling = MissingMemberHandling.Ignore;
                serializer.NullValueHandling = NullValueHandling.Include;
                serializer.DefaultValueHandling = DefaultValueHandling.Include;
                serializer.DateFormatString = "yyyy-MM-dd";
                serializer.Converters.Add(new StringEnumConverter());
            }

            public JsonNetSerializer(Newtonsoft.Json.JsonSerializer serializer)
            {
                ContentType = "application/json";
                this.serializer = serializer;
            }

            public string Serialize(object obj)
            {
                using (var stringWriter = new StringWriter())
                {
                    using (var jsonTextWriter = new JsonTextWriter(stringWriter))
                    {
                        jsonTextWriter.Formatting = Formatting.Indented;
                        jsonTextWriter.QuoteChar = '"';

                        serializer.Serialize(jsonTextWriter, obj);

                        var result = stringWriter.ToString();
                        return result;
                    }
                }
            }

            public string DateFormat { get; set; }
            public string RootElement { get; set; }
            public string Namespace { get; set; }
            public string ContentType { get; set; }
        }       
    }

    public static class Extentions
    {
        public static async Task TimeoutAfter(this Task task, int millisecondsTimeout)
        {
            if (task == await Task.WhenAny(task, Task.Delay(millisecondsTimeout)))
                await task;
            else
                throw new TimeoutException();
        }

    }
}