﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.EventHub.Client
{
    public interface IEventHubClient
    {
        Task<bool> Publish<T>(string eventName, T @event);

        void PublishBatchWithInterval<T>(List<T> events, int interval = 100);

        void On(string eventName, Action<EventInfo> handler);

        void Start();

        void StartAsync();

        Task<bool> Publish<T>(T @event);
    }
}