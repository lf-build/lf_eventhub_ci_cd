﻿namespace LendFoundry.EventHub.Client
{
    public interface IEventSubscriptionWrapper
    {
        string EventName { get; }

        void Handle(EventInfo @event);
    }
}