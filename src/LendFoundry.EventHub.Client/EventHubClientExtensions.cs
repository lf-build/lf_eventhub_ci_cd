﻿using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using System;
#if DOTNET2
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.AspNet.Builder;
using Microsoft.Framework.DependencyInjection;
#endif
namespace LendFoundry.EventHub.Client
{
    public static class EventHubClientExtensions
    {
        public static IServiceCollection AddEventHub(this IServiceCollection services, string endpoint, int port, string natsServer, string serviceName)
        {
            services.AddTransient<IEventHubClientFactory>(p => new EventHubClientFactory(p, endpoint, port, natsServer, serviceName));
            services.AddTransient(p => p.GetService<IEventHubClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddEventSubscription<TEvent, TSubscription>(this IServiceCollection services)
            where TSubscription : class, IEventSubscription<TEvent> where TEvent : class
        {
            services.AddTransient<IEventSubscription<TEvent>, TSubscription>();
            services.AddTransient<IEventSubscriptionWrapper>(p => new EventSubscriptionWrapper<TEvent>(p));
            return services;
        }

        public static void UseEventHub(this IApplicationBuilder app)
        {
            try
            {
                var wrappers = app.ApplicationServices.GetServices<IEventSubscriptionWrapper>();
                if (wrappers != null)
                {
                    var emptyReader = new StaticTokenReader(string.Empty);
                    var tokenHander = new TokenHandler(null, null, null);
                    var tenantFactory = app.ApplicationServices.GetService<ITenantServiceFactory>();
                    var tenantService = tenantFactory.Create(emptyReader);
                    var tenants = tenantService.GetActiveTenants();
                    tenants.ForEach(tenant =>
                    {
                        var token = tokenHander.Issue(tenant.Id, "eventhub");
                        var reader = new StaticTokenReader(token.Value);
                        var factory = app.ApplicationServices.GetService<IEventHubClientFactory>();
                        var hub = factory.Create(new StaticTokenReader(reader.Read()));
                        foreach (var wrapper in wrappers)
                        {
                            hub.On(wrapper.EventName, wrapper.Handle);
                            Console.WriteLine($"New subscription attached to tenant #{tenant.Id} event #{wrapper.EventName}");
                        }
                        hub.StartAsync();
                    });
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"UseEventHub extension cannot start: {ex.Message}");
            }
        }
    }
}