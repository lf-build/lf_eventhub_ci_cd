using System;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
#if DOTNET2
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.AspNet.Http;
using Microsoft.Framework.DependencyInjection;
#endif
using LendFoundry.Foundation.Services;

namespace LendFoundry.EventHub.Client
{
    public class EventHubClientFactory : IEventHubClientFactory
    {
        public EventHubClientFactory(IServiceProvider provider, string endpoint, int port, string natsServer, string serviceName)
        {
            Provider = provider;
            NetsServer = natsServer;
            ServiceName = serviceName;
            Endpoint = endpoint;
            Port = port;
        }

        private IServiceProvider Provider { get; }

        private string NetsServer { get; }

        private string ServiceName { get; }

        private string Endpoint { get; }

        private int Port { get; }

        private IEventHubListner lEventListner = null;

        private IEventHubListner EventListner
        {
            get
            {
                if (lEventListner == null)
                {
                    lEventListner = new EventHubListner(NetsServer, Provider.GetService<ILogger>(), ServiceName);
                }

                return lEventListner;
            }
        }
        private static readonly Object Lock = new object();
        public IEventHubClient Create(ITokenReader tokenReader)
        {
            var logger = Provider.GetService<ILogger>();
            var httpContextAccessor = Provider.GetService<IHttpContextAccessor>();            
            var client = Provider.GetServiceClient(tokenReader, Endpoint, Port);
            return new EventHubClient(client, tokenReader, EventListner, httpContextAccessor, logger, ServiceName);
        }
    }
}