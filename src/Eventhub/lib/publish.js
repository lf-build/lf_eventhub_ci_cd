const url = process.env.NATS_URL || 'nats://nats:4222';
const reconnectTimeout = process.env.RECONNECT_TIMEOUT || 5000;
const pubAckWait = process.env.PUB_ACK_WAIT || 60000;
const maxPubAcksInFlight = process.env.MAX_PUB_ACKS_IN_FLIGHT || 60000;
const nats = require('node-nats-streaming');
const uuid = require('uuid');
const debug = require('@sigma-infosolutions/orbit/debug')('event-hub:publish');

debug(`Using **${url}** with **${reconnectTimeout}ms** re-connection timeout after failure`);

let isReady = false;
let connection;
// Initiates connection
const connect = () => {
  debug('Initialized nats connection.');
  isReady = false;
  connection = nats.connect('test-cluster', uuid(), {
    url,
    pubAckWait,
    maxPubAcksInFlight,
  });

  // connect and close must be inside the connect method,
  // as each time connect is called, connection will be pointing
  // to diff connection and 'connect' and 'close' won't be triggered
  // for new Connects.
  connection.on('connect', () => {
    isReady = true;
    debug('Nats connected.');
  });
  connection.on('close', () => {
    connect();
    debug('nats disconnected for some reason. Reconnecting.');
  });
  connection.on('error', (err) => {
    debug(err);
    // Wait for few seconds then try to reconnect.
    setTimeout(connect, reconnectTimeout);
  });
};

// Trigger initial connection.
connect();

module.exports = (name, value, callback = () => {}) => {
  if (!isReady) {
    debug('Nats not ready, connection in progress.');
    callback(new Error('Nats not ready, connection in progress.'));
  }
  connection.publish(name, value, (err, guid) => {
    // Invoke the callback.
    callback(err, guid);

    // if an error occured, log it and do an early exit.
    if (err) {
      debug(`nats error publishing ${name}`, err);
      return;
    }

    debug(`event ${name} published with id: ${guid}`);
    // Publish the message to 'all'
    // All messages are published to 'all'
    // 'all' is like "*", we have to do it manually
    // due to limitation of nats-streaming.
    connection.publish('all', value);
  });
};
