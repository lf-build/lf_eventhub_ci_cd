const express = require('express');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const cors = require('cors');

const index = require('./routes/index');

const { healthcheck, token } = require('@sigma-infosolutions/orbit-base/middleware');

const app = express();

/* Refer: http://expressjs.com/en/guide/behind-proxies.html

 Summary:
 * Enabling trust proxy will have the following impact:
   The value of req.hostname is derived from the value set in the
   X-Forwarded-Host header, which can be set by the client or by the proxy.
 * X-Forwarded-Proto can be set by the reverse proxy to tell the app whether
   it is https or http or even an invalid name. This value is reflected by req.protocol.
 * The req.ip and req.ips values are populated with the list of addresses from X-Forwarded-For.
*/
app.enable('trust proxy');
app.use(cors());
// uncomment after placing your favicon in /public
app.use(logger('dev'));
app.use(bodyParser.json({ limit: '5mb' }));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

// LF related middle (from oribt-base)
app.use(healthcheck);

// Initialize req.body and req.local with empty objects.
app.use((req, res, next) => {
  if (!req.body) {
    req.body = {};
  }
  if (!req.local) {
    req.local = {};
  }
  next();
});
app.use(token);

app.use('/', index);

// catch 404 and forward to error handler
app.use((req, res, next) => {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use((err, req, res, next) => { // eslint-disable-line
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.json({ ...res.locals });
});

module.exports = app;
