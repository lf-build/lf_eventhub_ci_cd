const express = require('express');
const uuid = require('uuid');
const publish = require('../lib/publish');

const router = express.Router();

/* Publish event route */
router.post('/:event', (req, res, next) => { // eslint-disable-line
  const { tenant: tenantId, iss: source, sub: userName } = req.user;
  // Generate unique request ID.
  const id = uuid();

  // Extract ip address. If the request was forwarded and forwared by some reverse proxy,
  // we preffer x-forwarded-for a higher priority.
  const ipAddress = req.ip || '';

  // Extract event name from params.
  // We need not check for event name === null as
  // if event name is missing, it will not hit this route
  // and automatically respond with 404.
  const name = req.params.event;

  // Extract event payload.
  // Directly using req.body as its already a JSON.
  const data = req.body;

  // Initializing timebucket.
  const time = {
    time: new Date().toLocaleString(),
    day: null,
    hour: null,
    month: null,
    quarter: null,
    week: null,
    year: null,
  };

  const eventInfo = {
    tenantId,
    id,
    name,
    source,
    time,
    userName,
    token: req.token,
    data,
    ipAddress,
  };

  // Publish the event.
  // only string information can be published.
  // Hence its required that we stringify the eventInfo.
  publish(name, JSON.stringify(eventInfo), (err) => {
    // If anything went wrong, err will have some explaination
    // if so, we extract message and do an early exit.
    if (err) {
      res.status(500).json({ message: err.message || err });
      return;
    }

    res.status(204).send();
  });
});


module.exports = router;
