﻿using LendFoundry.Foundation.Date;

namespace LendFoundry.EventHub
{
    public interface IEventInfo
    {        
        /// <summary>
        /// IPAddress from where event is published
        /// </summary>
        string IpAddress { get; set; }

        /// <summary>
        /// token who published the event
        /// </summary>
        string Token { get; set; }

        /// <summary>
        /// Subject from token who published the event
        /// </summary>
        string UserName { get; set; }

        /// <summary>
        /// Tenant reference
        /// </summary>
        string TenantId { get; set; }

        /// <summary>
        /// Correlation ID used to track last sent event
        /// </summary>
        string Id { get; set; }

        /// <summary>
        /// Name of the event
        /// </summary>        
        string Name { get; set; }

        /// <summary>
        /// Time that the event has been published
        /// </summary>
        TimeBucket Time { get; set; }

        /// <summary>
        /// Time that the event has been published
        /// </summary>
        string Source { get; set; }

        /// <summary>
        /// The data payload included with the event
        /// </summary>        
        object Data { get; set; }
    }
}
