using LendFoundry.Foundation.Date;
using System;

namespace LendFoundry.EventHub
{
    [Serializable]
    public class Event : IEventInfo
    {
        /// <summary>
        /// IPAddress from where event is published
        /// </summary>
        public string IpAddress { get; set; }

        /// <summary>
        /// token who published the event
        /// </summary>
        public string Token { get; set; }

        /// <summary>
        /// Subject from token who published the event
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// Tenant reference
        /// </summary>
        public string TenantId { get; set; }
        /// <summary>
        /// Correlation ID used to track last sent event
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// Name of the event
        /// </summary>        
        public string Name { get; set; }
        /// <summary>
        /// Time that the event has been published
        /// </summary>
        public TimeBucket Time { get; set; }
        /// <summary>
        /// Time that the event has been published
        /// </summary>
        public string Source { get; set; }
        /// <summary>
        /// The data payload included with the event
        /// </summary>        
        public object Data { get; set; }
    }
}
