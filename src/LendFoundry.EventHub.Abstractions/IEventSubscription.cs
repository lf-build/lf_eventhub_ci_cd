namespace LendFoundry.EventHub
{ 
    public interface IEventSubscription<in T> where T : class
    {
        void Handle(IEventInfo @event, T data);
    }
}