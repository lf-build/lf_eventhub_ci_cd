﻿using System;
using LendFoundry.Security.Tokens;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.Foundation.Logging;
using Moq;
using Xunit;
using System.Collections.Generic;
using LendFoundry.Foundation.Services;
#if DOTNET2
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.AspNet.Builder;
using Microsoft.Framework.DependencyInjection;
#endif
namespace LendFoundry.EventHub.Client.Tests
{
    public class EventHubClientTest
    {
        public string Server => "idea.dev.lendfoundry.com:4222";
        public string EventHubHost = "idea.dev.lendfoundry.com";
        public int EventHubPort = 7002;
        public string serviceName = "activity-log";

        [Fact]
        public void HardTest_ListenAll_Amazon()
        {
            IEventHubListner listner = new EventHubListner(Server, Mock.Of<ILogger>(), serviceName);
            var tokenHandler = new TokenHandler(null, null, null);
            var token = tokenHandler.Issue("my-tenant", "my-app");
            var reader = new StaticTokenReader(token.Value);
            var logger = Mock.Of<ILogger>();
            IServiceClient serviceClient = new ServiceClient
            (
                  accessor: new EmptyHttpContextAccessor(),
                  logger: Mock.Of<ILogger>(),
                  tokenReader: reader,
                  endpoint: EventHubHost,
                  port: EventHubPort
            );

            // client
            //var client = new EventHubClient(serviceClient, reader, Server, null, logger);
            var client = new EventHubClient(serviceClient, reader, listner, null, logger, serviceName);
            client.On("all", ei =>
            {
                Debug.WriteLine($"RECEIVED => {ei.Name} TENANT => {ei.TenantId} DATA => {ei.Data}");
            });
            //client.StartAsync();
            Task.Delay(1000 * 60).Wait();
        }

        [Fact]
        public void HardTest_MultiConnectionsLike_Listener()
        {
            IEventHubListner listner = new EventHubListner(Server, Mock.Of<ILogger>(), serviceName);
            var tokenHandler = new TokenHandler(null, null, null);
            var token = tokenHandler.Issue("my-tenant", "my-app");
            var reader = new StaticTokenReader(token.Value);
            var logger = Mock.Of<ILogger>();
            IServiceClient serviceClient = new ServiceClient
            (
                  accessor: new EmptyHttpContextAccessor(),
                  logger: Mock.Of<ILogger>(),
                  tokenReader: reader,
                  endpoint: EventHubHost,
                  port: EventHubPort
            );

            // client
            //var client = new EventHubClient(serviceClient, reader, Server, null, logger);
            var client = new EventHubClient(serviceClient, reader, listner, null, logger, serviceName);
            new[] { "ApplicantLoggedIn", "MerchantStatusChanged", "BusinessReportAdded", "OfferGenerated", "OfferSelected", "NoteAdded", "ApplicationStatusChanged", "CreditReportAdded", "SyndicationCalledEvent", "ApplicantNameModified", "ApplicantAddressModified", "ApplicantPhoneModified", "ApplicationCreated", "ApplicationDeleted", "ApplicationModified", "ApplicationOptedInForAutoPayment", "DocumentDeleted", "LoanOnboarded", "PaymentAdded", "PaymentReceived", "PaymentFailed", "LoanStatusUpdated", "PaymentCancelled", "LoanActivityLogged", "AchInstructionRejected", "NotificationDelivered", "LoanConsentAccepted" }
            .ToList().ForEach(e =>
            {
                client.On(e, ei =>
                {
                    Debug.WriteLine($"RECEIVED => {ei.Name} TENANT => {ei.TenantId} DATA => {ei.Data}");
                });
            });
            //client.StartAsync();
            Task.Delay(1000 * 60).Wait();
        }

        [Fact]
        public void HardTest_Amazon()
        {
            IEventHubListner listner = new EventHubListner(Server, Mock.Of<ILogger>(), serviceName);
            var tokenHandler = new TokenHandler(null, null, null);
            var token = tokenHandler.Issue("my-tenant", "my-app");
            var reader = new StaticTokenReader(token.Value);
            var logger = Mock.Of<ILogger>();
            IServiceClient serviceClient = new ServiceClient
            (
                  accessor: new EmptyHttpContextAccessor(),
                  logger: Mock.Of<ILogger>(),
                  tokenReader: reader,
                  endpoint: EventHubHost,
                  port: EventHubPort
            );

            // client
            //var client = new EventHubClient(serviceClient, reader, Server, null, logger);
            var client = new EventHubClient(serviceClient, reader, listner, null, logger, serviceName);

            client.On("event-a", ei =>
            {
                Debug.WriteLine($"RECEIVED => {ei.Name} TENANT => {ei.TenantId} DATA => {ei.Data}");
            });
            //client.StartAsync();

            new[] { "event-a" }.ToList().ForEach(async (ev) =>
            {
                await client.Publish(ev, new { age = 10 });
            });
            Task.Delay(1000 * 60).Wait();
        }

        [Fact]
        public void HardTest_Server_1()
        {
            IEventHubListner listner = new EventHubListner(Server, Mock.Of<ILogger>(), serviceName);
            var tokenHandler = new TokenHandler(null, null, null);
            var token = tokenHandler.Issue("my-tenant", "my-app");
            var reader = new StaticTokenReader(token.Value);
            var logger = Mock.Of<ILogger>();
            var serviceClient = Mock.Of<IServiceClient>();

            // client
            //var client = new EventHubClient(serviceClient, reader, Server, null, logger);
            var client = new EventHubClient(serviceClient, reader, listner, null, logger, serviceName);

            client.On("event-a", ei =>
            {
                Debug.WriteLine($"RECEIVED => {ei.Name} TENANT => {ei.TenantId} DATA => {ei.Data}");
            });
            //client.StartAsync();

            new[] { "event-a" }.ToList().ForEach(async (ev) =>
            {
                await client.Publish(ev, new { age = 10 });
            });
            Task.Delay(1000 * 60).Wait();
        }

        [Fact]
        public void Test_MultiClientsListening_BeforeAndAfter_Publish()
        {
            IEventHubListner listner = new EventHubListner(Server, Mock.Of<ILogger>(), serviceName);
            var listenedEvent = new List<string>();
            var tokenHandler = new TokenHandler(null, null, null);
            var token = tokenHandler.Issue("my-tenant", "my-app");
            var reader = new StaticTokenReader(token.Value);
            var logger = Mock.Of<ILogger>();
            var serviceClient = Mock.Of<IServiceClient>();

            // Set of clients
            var clientA = new EventHubClient(serviceClient, reader, listner, null, logger, serviceName);
            var clientB = new EventHubClient(serviceClient, reader, listner, null, logger, serviceName);
            var clientAll = new EventHubClient(serviceClient, reader, listner, null, logger, serviceName);
            var clientPublisher = new EventHubClient(serviceClient, reader, listner, null, logger, serviceName);

            // Client A subscribes event-a and get connected before it published
            clientA.On("event-a", ei =>
            {
                Debug.WriteLine($"{nameof(clientA)} RECEIVED EVENT #{ei.Name}");
            });
            //clientA.StartAsync();

            // Client B subscribes event-b and get connected before it published          
            clientB.On("event-b", ei =>
            {
                Debug.WriteLine($"{nameof(clientB)} RECEIVED EVENT #{ei.Name}");
            });
            //clientB.StartAsync();

            // Publish all expected events
            new[] { "event-a", "event-b" }.ToList().ForEach(async (ev) =>
            {
                await clientPublisher.Publish(ev, new { date = DateTime.Now });
            });

            // Client All subscribes after all messages published but must to receive all info        
            clientAll.On("all", ei =>
            {
                Debug.WriteLine($"{nameof(clientAll)} RECEIVED EVENT #{ei.Name}");
            });
            //clientAll.StartAsync();

            Task.Delay(1000 * 60).Wait();
        }

        [Fact]
        public void Test_ListeningAllEvents()
        {
            IEventHubListner listner = new EventHubListner(Server, Mock.Of<ILogger>(),serviceName);
            var tokenHandler = new TokenHandler(null, null, null);
            var token = tokenHandler.Issue("my-tenant", "my-app");
            var reader = new StaticTokenReader(token.Value);
            var logger = Mock.Of<ILogger>();
            var serviceClient = Mock.Of<IServiceClient>();

            // Eventhub instances            
            //var eventhub = new EventHubClient(serviceClient, reader, Server, null, logger);
            var eventhub = new EventHubClient(serviceClient, reader, listner, null, logger, serviceName);
            eventhub.On("all", ei =>
            {
                Debug.WriteLine($"RECEIVED => {ei.Name} TENANT => {ei.TenantId}");
            });
            eventhub.StartAsync();
            Task.Delay(1000 * 240).Wait();
        }
        [Fact]
        public void Test_Multiple_Tenant_NEW()
        {
            var logger = Mock.Of<ILogger>();

            IEventHubListner listner = new EventHubListner(Server, logger, "activity-log");

            // Token generation
            //My Tenant
            {

                // Eventhub instances                

                listner.On("event-a", "my-tenant-1", ei =>
                {
                    if (ei.TenantId != "my-tenant-1")
                        throw new Exception("It received event for another tenant:" + ei.TenantId);

                    Trace.WriteLine($"RECEIVED => {ei.Name} TENANT => {ei.TenantId} DATA => {ei.Data} SUBSCRIBED => my-tenant event-a");
                });

                listner.On("event-b", "my-tenant-1", ei =>
                {
                    if (ei.TenantId != "my-tenant-1")
                        throw new Exception("It received event for another tenant:" + ei.TenantId);

                    Trace.WriteLine($"RECEIVED => {ei.Name} TENANT => {ei.TenantId} DATA => {ei.Data} SUBSCRIBED => my-tenant event-a");
                });

                listner.On("event-c", "my-tenant-1", ei =>
                {
                    if (ei.TenantId != "my-tenant-1")
                        throw new Exception("It received event for another tenant:" + ei.TenantId);

                    Trace.WriteLine($"RECEIVED => {ei.Name} TENANT => {ei.TenantId} DATA => {ei.Data} SUBSCRIBED => my-tenant event-a");
                });
            }

            {
                listner.On("event-a", "my-tenant-2", ei =>
                {
                    if (ei.TenantId != "my-tenant-2")
                        throw new Exception("It received event for another tenant:" + ei.TenantId);

                    Trace.WriteLine($"RECEIVED => {ei.Name} TENANT => {ei.TenantId} DATA => {ei.Data} SUBSCRIBED => my-tenant2 event-a");
                });
            }

            {
                listner.On("event-a", "my-tenant-3", ei =>
                {
                    if (ei.TenantId != "my-tenant-3")
                        throw new Exception("It received event for another tenant:" + ei.TenantId);

                    Trace.WriteLine($"RECEIVED => {ei.Name} TENANT => {ei.TenantId} DATA => {ei.Data} SUBSCRIBED => my-tenant3 event-a");
                });
            }

            Task.Delay(100000 * 240).Wait();
        }


        [Fact]
        public void Test_Multiple_Tenant()
        {
            IEventHubListner listner = new EventHubListner(Server, Mock.Of<ILogger>(), serviceName);
            EventHubClient myTenantEventHub;
            EventHubClient my2TenantEventHub;

            // Token generation
            //My Tenant
            {
                var tokenHandler = new TokenHandler(null, null, null);
                var token = tokenHandler.Issue("my-tenant", "my-app");
                var reader = new StaticTokenReader(token.Value);
                var logger = Mock.Of<ILogger>();
                var serviceClient = Mock.Of<IServiceClient>();

                // Eventhub instances                
                myTenantEventHub = new EventHubClient(serviceClient, reader, listner, null, logger, serviceName);

                myTenantEventHub.On("event-a", ei =>
                {
                    if (ei.TenantId != "my-tenant")
                        throw new Exception("It received event for another tenant:" + ei.TenantId);

                    Console.WriteLine($"RECEIVED => {ei.Name} TENANT => {ei.TenantId} DATA => {ei.Data} SUBSCRIBED => my-tenant event-a");
                });
                myTenantEventHub.On("all", ei =>
                {
                    if (ei.TenantId != "my-tenant")
                        throw new Exception("It received event for another tenant:" + ei.TenantId);

                    Console.WriteLine($"RECEIVED => {ei.Name} TENANT => {ei.TenantId} DATA => {ei.Data} SUBSCRIBED => my-tenant all");
                });
               // myTenantEventHub.StartAsync();
            }

            {
                var tokenHandler = new TokenHandler(null, null, null);
                var token = tokenHandler.Issue("my-tenant2", "my-app");
                var reader = new StaticTokenReader(token.Value);
                var logger = Mock.Of<ILogger>();
                var serviceClient = Mock.Of<IServiceClient>();

                // Eventhub instances                
                my2TenantEventHub = new EventHubClient(serviceClient, reader, listner, null, logger, serviceName);

                my2TenantEventHub.On("event-a", ei =>
                {
                    if (ei.TenantId != "my-tenant2")
                        throw new Exception("It received event for another tenant:" + ei.TenantId);

                    Console.WriteLine($"RECEIVED => {ei.Name} TENANT => {ei.TenantId} DATA => {ei.Data} SUBSCRIBED => my-tenant2 event-a");
                });
                my2TenantEventHub.On("all", ei =>
                {
                    if (ei.TenantId != "my-tenant2")
                        throw new Exception("It received event for another tenant:" + ei.TenantId);

                    Console.WriteLine($"RECEIVED => {ei.Name} TENANT => {ei.TenantId} DATA => {ei.Data} SUBSCRIBED => my-tenant2 all events");
                });
                //my2TenantEventHub.StartAsync();
            }

            Task.Delay(1000 * 240).Wait();
        }

        [Fact]
        public void Test_MakeSureEventPublishedOnlyOnceIfHandlerIsNotTakingMuchTime()
        {
            IEventHubListner listner = new EventHubListner(Server, Mock.Of<ILogger>(), serviceName);
            var tokenHandler = new TokenHandler(null, null, null);
            var token = tokenHandler.Issue("my-tenant", "my-app");
            var reader = new StaticTokenReader(token.Value);
            var logger = Mock.Of<ILogger>();
            var serviceClient = new ServiceClient(new EmptyHttpContextAccessor(), Mock.Of<ILogger>(), reader,
                EventHubHost, EventHubPort);

            var clientPublisher = new EventHubClient(serviceClient, reader, listner, null, logger, serviceName);

            // Eventhub instances            
            var eventhub = new EventHubClient(serviceClient, reader, listner, null, logger, serviceName);
            var ids = new List<string>();
            eventhub.On("event-a", ei =>
            {
                ids.Add(ei.Id);
                Debug.WriteLine($"RECEIVED => {ei.Name} TENANT => {ei.TenantId}");
            });

            eventhub.StartAsync();
            
            //wait for 10 seconds to finish initialization of client
            Task.Delay(10000).Wait();

            clientPublisher.Publish("event-a", new { value = "test" }).Wait();

            Task.Delay(1000 * 60).Wait();

            Assert.True(ids.Count ==1);
        }
        
        [Fact]
        public void Test_MakeSureEventPublishedOnlyTwiceIfTakingLongtime()
        {
            IEventHubListner listner = new EventHubListner(Server, Mock.Of<ILogger>(), serviceName);
            var tokenHandler = new TokenHandler(null, null, null);
            var token = tokenHandler.Issue("my-tenant", "my-app");
            var reader = new StaticTokenReader(token.Value);
            var logger = Mock.Of<ILogger>();
            var serviceClient = new ServiceClient(new EmptyHttpContextAccessor(), Mock.Of<ILogger>(), reader,
                EventHubHost, EventHubPort);
            var clientPublisher = new EventHubClient(serviceClient, reader, listner, null, logger, serviceName);

            // Eventhub instances            
            var eventhub = new EventHubClient(serviceClient, reader, listner, null, logger, serviceName);
            var ids = new List<string>();
            eventhub.On("event-a", ei =>
            {
                ids.Add(ei.Id);
                Debug.WriteLine($"RECEIVED => {ei.Name} TENANT => {ei.TenantId}");
                Task.Delay(60005).Wait();
            });
            
            eventhub.StartAsync();
            //wait for 10 seconds to finish initialization of client
            Task.Delay(10000).Wait();
            
            clientPublisher.Publish("event-a", new {value = "test"}).Wait();

            Task.Delay(1000 * 360).Wait();

            Assert.True(ids.Count() == 2);
        }
    }
}
