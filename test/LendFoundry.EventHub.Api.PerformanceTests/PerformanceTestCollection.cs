﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Xunit;

namespace LendFoundry.EventHub.Api.PerformanceTests
{
    public class PerformanceTestCollection
    {
        string _endpoint = string.Empty;

        public PerformanceTestCollection()
        {
            _endpoint = "localhost";
        }

        void LockSeconds(int seconds = 1)
        {
            // just lock the main thread to see the callback results.
            var time = (seconds * 1000);
            Debug.WriteLine($"Main thread locked for {seconds} seconds");
            Task.Delay(time).Wait();
        }

        void LockMinutes(int minutes = 1)
        {
            // just lock the main thread to see the callback results.
            var time = (minutes * 60000);
            Debug.WriteLine($"Main thread locked for {minutes} minutes");
            Task.Delay(time).Wait();
        }

        [Fact(Skip = "Not implemented")]
        public async void Client_MultiplePublishConnections_MultiTopic_Test()
        {
            //Random rand = new Random();
            //List<EventHubClient> publishers = new List<EventHubClient>();
            //for (int i = 0; i < 100; i++)
            //{
            //    publishers.Add(new EventHubClient(_endpoint));
            //}

            //await Task.Run(() => Parallel.ForEach(publishers, hub =>
            //{
            //    var data = new
            //    {
            //        name = "John Mayer",
            //        age = 23,
            //        city = "Bohemia",
            //        state = "New York"
            //    };
            //    var res = hub.Publish(string.Format("topic{0}", rand.Next(1, 5).ToString()), data);
            //    if (res == false)
            //        throw new Exception("Problem found on sequencial batch publisher.");
            //}));
            throw new NotImplementedException();
        }

        [Fact(Skip = "Not implemented")]
        public async void Client_MultiplePublishConnections_SingleTopic_Test()
        {
            //Random rand = new Random();
            //List<EventHubClient> publishers = new List<EventHubClient>();
            //for (int i = 0; i < 100; i++)
            //{
            //    publishers.Add(new EventHubClient(_endpoint));
            //}

            //await Task.Run(() => Parallel.ForEach(publishers, hub =>
            //{
            //    var data = new
            //    {
            //        name = "John Mayer",
            //        age = 23,
            //        city = "Bohemia",
            //        state = "New York"
            //    };
            //    var res = hub.Publish("topic1", data);
            //    if (res == false)
            //        throw new Exception("Problem found on sequencial batch publisher.");
            //}));
            throw new NotImplementedException();
        }

        [Fact(Skip = "Not implemented")]
        public async void Client_SinglePublishConnection_MultiTopic_Test()
        {
            //Random rand = new Random();
            //var hub = new EventHubClient(_endpoint);
            //List<dynamic> listTopic = new List<dynamic>();
            //for (int i = 0; i < 20; i++)
            //{
            //    listTopic.Add(new
            //    {
            //        name = "John Mayer",
            //        age = 23,
            //        city = "Bohemia",
            //        state = "New York"
            //    });
            //}

            //await Task.Run(() => Parallel.ForEach(listTopic, t =>
            //{
            //    var res = hub.Publish(string.Format("topic{0}", rand.Next(2, 5).ToString()), t);
            //    if (res == false)
            //        throw new Exception("Problem found on sequencial batch publisher.");
            //}));
            throw new NotImplementedException();
        }

        [Fact(Skip = "Not implemented")]
        public void Client_SinglePublishConnection_SingleTopic_Test()
        {
            //var hub = new EventHubClient(_endpoint);
            //Random rand = new Random();
            //List<object> listTopic = new List<object>();
            //for (int i = 0; i < 10; i++)
            //{
            //    listTopic.Add(new
            //    {
            //        name = "John Mayer",
            //        age = 23,
            //        city = "Bohemia",
            //        state = "New York"
            //    });
            //}

            //Parallel.ForEach(listTopic, tp =>
            //{
            //    var res = hub.Publish("topic1", tp);
            //    if (res == false)
            //        throw new Exception("Problem found on sequencial batch publisher.");
            //});
            throw new NotImplementedException();
        }

        [Fact(Skip = "Not implemented")]
        public void Client_SingleSubscriberConnection_Test()
        {
            //try
            //{
            //    List<TimeSpan> list = new List<TimeSpan>();
            //    var hub = new EventHubClient(_endpoint);
            //    Debug.WriteLine("-------Session test STARTED-------");
            //    Stopwatch Watch = new Stopwatch();
            //    hub.On("topic1", (msg) =>
            //    {
            //        Watch.Stop();
            //        list.Add(Watch.Elapsed);
            //        if (string.IsNullOrEmpty(msg.Data.ToString()) == false)
            //        {
            //            Debug.WriteLine(string.Format("message id [{0}] received in {1}", msg.Id, DateTime.Now.Millisecond));
            //        }
            //        Watch = new Stopwatch();
            //        Watch.Start();
            //    });

            //    while (list.Count < 100) { }

            //    Debug.WriteLine("EventHub message response time avg: " + list.Average(a => a.Milliseconds) + " milliseconds");
            //    Debug.WriteLine("-------Session test DONE-------");
            //}
            //catch (WebSocketException)
            //{
            //    Debug.WriteLine("EventHub has dropped the connection.");
            //}
            throw new NotImplementedException();
        }

        [Fact(Skip = "Not implemented")]
        public async void WebApi_MultiplePublishConnections_Test()
        {
            //List<Tuple<EventHubClient, string, object>> publishers = new List<Tuple<EventHubClient, string, object>>();
            //Random rand = new Random();
            //for (int i = 0; i < 100; i++)
            //{
            //    publishers.Add(new Tuple<EventHubClient, string, object>(new EventHubClient(_endpoint),
            //                   string.Format("topic{0}", rand.Next(1, 50).ToString()),
            //    new
            //    {
            //        name = "John Mayer",
            //        age = 23,
            //        city = "Bohemia",
            //        state = "New York"
            //    }));
            //}

            //await Task.Run(() => Parallel.ForEach(publishers, p =>
            //{
            //    var hub = p.Item1;
            //    string eventName = p.Item2;
            //    object data = p.Item3;
            //    var res = hub.Publish(eventName, JsonConvert.SerializeObject(data));
            //    if (res == false)
            //        throw new Exception("Problem found on sequencial batch publisher.");
            //}));
            throw new NotImplementedException();
        }

        [Fact]
        public void ApiCoreSubscriber_Stress_Test()
        {
            //var f = new NsqQueueFactory(new HostConfiguration
            //{
            //    PublisherHost = "127.0.0.1:4150",
            //    SubscriberHost = "127.0.0.1:4161",
            //});

            //Debug.WriteLine("------- EventHub Test - Subscriber -------");
            //Debug.WriteLine("Session started in: " + DateTime.Now);
            //var sub = f.CreateSubscriber("topic1");

            //List<TimeSpan> list = new List<TimeSpan>();
            //Stopwatch Watch = new Stopwatch();
            //sub.OnReceive += (sender, e) =>
            //{
            //    Watch.Stop();
            //    list.Add(Watch.Elapsed);
            //    Event msg = (sender as Event);
            //    Debug.WriteLine(string.Format("message id [{0}] sent in {1} milliseconds", msg.Id, Watch.Elapsed));

            //    if (list.Count > 4999)
            //    {
            //        Debug.WriteLine(string.Format("Processed messages [{0}] | Best time [{1}] | Worse time [{2}]",
            //                    list.Count, list.Where(a => a.Milliseconds > 0).Min(a => a.Milliseconds), list.Max(a => a.Milliseconds)));
            //        Debug.WriteLine("Session finished in: " + DateTime.Now);
            //    }

            //    Watch = new Stopwatch();
            //    Watch.Start();
            //};
            //Watch.Start();
            //sub.StartListen();
        }

        [Fact]
        public void ApiCorePublisher_Stress_Test()
        {
            //var f = new NsqQueueFactory(new HostConfiguration
            //{
            //    PublisherHost = "127.0.0.1:4150",
            //    SubscriberHost = "127.0.0.1:4161",
            //});

            //var pub = f.CreatePublisher();
            //var listTopic = new List<string>();

            //// create topics
            //for (int i = 1; i < 10; i++)
            //    listTopic.Add($"topic{i}");

            //// publish topic
            //Parallel.ForEach(listTopic, tname =>
            //{
            //    for (int i = 0; i < 10; i++)
            //    {
            //        Task.Delay(500).Wait();
            //        pub.Publish(tname, new
            //        {
            //            firstName = "Bill",
            //            lastName = "DiCapua",
            //            age = 30
            //        });
            //    }
            //});

            //foreach (var tname in listTopic)
            //{
            //    for (int i = 0; i < 100; i++)
            //    {
            //        Task.Delay(200).Wait();
            //        pub.Publish(tname, new
            //        {
            //            firstName = "Bill",
            //            lastName = "DiCapua",
            //            age = 30
            //        });
            //    }
            //}

            Assert.True(true);
        }

        [Fact(Skip = "Not implemented")]
        public async void When_ClientUsingMultipleListenersOk_Then_Success()
        {
            //// Instance of hub client that handle a proxy with informed endpoint.
            //var hub = new EventHubClient(_endpoint);

            //Debug.WriteLine("------- Test Start -------");

            //// List of subscribers for events.
            //hub.On("topic1", msg => { Debug.WriteLine($"message received from [{"topic1"}] messageId [{msg.Id}]"); });
            //hub.On("topic2", msg => { Debug.WriteLine($"message received from [{"topic2"}] messageId [{msg.Id}]"); });
            //hub.On("topic3", msg => { Debug.WriteLine($"message received from [{"topic3"}] messageId [{msg.Id}]"); });
            //hub.On("topic4", msg => { Debug.WriteLine($"message received from [{"topic4"}] messageId [{msg.Id}]"); });
            //hub.On("topic5", msg => { Debug.WriteLine($"message received from [{"topic5"}] messageId [{msg.Id}]"); });
            //hub.On("topic6", msg => { Debug.WriteLine($"message received from [{"topic6"}] messageId [{msg.Id}]"); });
            //hub.On("topic7", msg => { Debug.WriteLine($"message received from [{"topic7"}] messageId [{msg.Id}]"); });
            //hub.On("topic8", msg => { Debug.WriteLine($"message received from [{"topic8"}] messageId [{msg.Id}]"); });
            //hub.On("topic9", msg => { Debug.WriteLine($"message received from [{"topic9"}] messageId [{msg.Id}]"); });

            //// Just to lock the current thread. When testing please uncomment this.
            //await Task.Delay((60 * 1000) * 10);
            //Assert.True(true);
            throw new NotImplementedException();
        }

        [Fact(Skip = "Not implemented")]
        public void Subscriber_MultiTopic_Listener()
        {
            //var hub = new EventHubClient(_endpoint);
            //hub.On("*", e =>
            //{
            //    Debug.WriteLine($"ALL EVENT (*)  >> message id [{e.Id}] received from event [{e.Name}]");
            //});
            //hub.Disconnected += (sender, e) =>
            //{
            //    Debug.WriteLine("Eventhub connection dropped: " + e.Message);
            //};
            //LockMinutes(30);
            throw new NotImplementedException();
        }

        [Fact]
        public void SinglePublish_NsqSharp_DirectApiCore()
        {
            //var hub = new NsqSharpPublisher(new HostConfiguration
            //{
            //    PublisherHost = "localhost",
            //    PublisherPort = "4150",
            //    SubscriberHost = "localhost",
            //    SubscriberPort = "4161"
            //});

            //hub.Publish<object>("topic1", new
            //{
            //    age = 30
            //});

            //LockMinutes(10);
        }

        [Fact]
        public void SingleSubscription_NsqSharp_DirectApiCore()
        {
            //var hub = new NsqSharpSubscriber(new HostConfiguration
            //{
            //    PublisherHost = "localhost",
            //    PublisherPort = "4150",
            //    SubscriberHost = "localhost",
            //    SubscriberPort = "4161"
            //}, "topic1", "channel-a");
            //hub.OnReceive += (sender, er) =>
            //{
            //    var e = sender as Event;
            //    Debug.WriteLine($"message id [{e.Id}] received from event [{e.Name}] body [{e.Data.ToString()}]");
            //};
            //hub.StartListen();

            //LockMinutes(10);
        }

        [Fact]
        public void MultiSubscription_NsqSharp_DirectApiCore()
        {
            //Task.Factory.StartNew(() =>
            //{
            //    string eventName = "topic1";
            //    string channelName = "channel-a";
            //    var hub = new NsqSharpSubscriber(new HostConfiguration
            //    {
            //        PublisherHost = "localhost",
            //        PublisherPort = "4150",
            //        SubscriberHost = "localhost",
            //        SubscriberPort = "4161"
            //    }, eventName, channelName);
            //    hub.OnReceive += (sender, er) =>
            //    {
            //        var e = sender as Event;
            //        Debug.WriteLine($"MSGID => [{e.Id}] TOPIC=> [{eventName}] CHANNEL=> [{channelName}] BODY=> [{e.Data.ToString()}]");
            //    };
            //    hub.StartListen();
            //});

            //Task.Factory.StartNew(() =>
            //{
            //    string eventName = "topic1";
            //    string channelName = "channel-b";
            //    var hub = new NsqSharpSubscriber(new HostConfiguration
            //    {
            //        PublisherHost = "localhost",
            //        PublisherPort = "4150",
            //        SubscriberHost = "localhost",
            //        SubscriberPort = "4161"
            //    }, eventName, channelName);
            //    hub.OnReceive += (sender, er) =>
            //    {
            //        var e = sender as Event;
            //        Debug.WriteLine($"MSGID => [{e.Id}] TOPIC=> [{eventName}] CHANNEL=> [{channelName}] BODY=> [{e.Data.ToString()}]");
            //    };
            //    hub.StartListen();
            //});

            //Task.Factory.StartNew(() =>
            //{
            //    string eventName = "topic1";
            //    string channelName = "channel-c";
            //    var hub = new NsqSharpSubscriber(new HostConfiguration
            //    {
            //        PublisherHost = "localhost",
            //        PublisherPort = "4150",
            //        SubscriberHost = "localhost",
            //        SubscriberPort = "4161"
            //    }, eventName, channelName);
            //    hub.OnReceive += (sender, er) =>
            //    {
            //        var e = sender as Event;
            //        Debug.WriteLine($"MSGID => [{e.Id}] TOPIC=> [{eventName}] CHANNEL=> [{channelName}] BODY=> [{e.Data.ToString()}]");
            //    };
            //    hub.StartListen();
            //});

            //LockMinutes(20);
        }
    }
}
