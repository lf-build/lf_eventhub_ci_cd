﻿using Xunit;

namespace LendFoundry.EventHub.Api.Tests
{
    public class EventhubControllerTest
    {
        //private Mock<IQueueConfiguration> Configuration { get; }

        //public EventhubControllerTest()
        //{
        //    Configuration = new Mock<IQueueConfiguration>();
        //}

        [Fact(Skip = "Needs refactoring")]
        public void When_PublishCorrectInfoEntry_Then_Success()
        {
            // TODO:
            //// Arrange                        
            //ILogger logger = Mock.Of<ILogger>();            
            //_configuration.Setup(c => c.PublisherHost).Returns("localhost");
            //_configuration.Setup(c => c.PublisherPort).Returns("5000");

            //// Action
            //var act = new EventHubController(logger, _configuration.Object);
            //var res = act.Publish("foo", new { data = "json" });

            //// Assert                        
            //Assert.True(((HttpStatusCodeResult)res).StatusCode == 200);
        }

        [Fact]
        public void When_PublishEventNameMaxlimitExceeded_Then_Fail()
        {
            //// Arrange   
            //var logger = Mock.Of<ILogger>();
            //var bigname = "qwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiop";
            //Configuration.Setup(c => c.PublisherHost).Returns("localhost:8080");

            //// Action
            //var controller = new EventHubController(logger, Configuration.Object);
            //var result = controller.Publish(bigname, null) as ErrorResult;

            //Assert.NotNull(result);
            //Assert.Equal(400, result.StatusCode);

            //var error = result.Value as Error;
            //Assert.NotNull(error);
            //Assert.Equal("Max length event name exceeded [64 characters].", error.Message);
        }

        [Fact]
        public void When_PublishEventNameEmptyValue_Then_Fail()
        {
            //// Arrange   
            //var logger = Mock.Of<ILogger>();
            //Configuration.Setup(c => c.PublisherHost).Returns("localhost:8080");

            //// Action
            //var controller = new EventHubController(logger, Configuration.Object);
            //var result = controller.Publish("", null) as ErrorResult;

            //Assert.NotNull(result);
            //Assert.Equal(400, result.StatusCode);

            //var error = result.Value as Error;
            //Assert.NotNull(error);
            //Assert.Equal("Event name must be informed to performs this operation.", error.Message);
        }

        [Fact]
        public void When_PublishEventNameInitialInvalidCharacter_Then_Fail()
        {
            //// Arrange  
            //var eventname = "%$$$ABC123";
            //var logger = Mock.Of<ILogger>();
            //Configuration.Setup(c => c.PublisherHost).Returns("localhost:8080");

            //// Action
            //var controller = new EventHubController(logger, Configuration.Object);
            //var result = controller.Publish(eventname, null) as ErrorResult;

            //Assert.NotNull(result);
            //Assert.Equal(400, result.StatusCode);

            //var error = result.Value as Error;
            //Assert.NotNull(error);
            //Assert.Equal("Invalid characters found in event name, please review it [.a-zA-Z0-9_-].", error.Message);
        }

        [Fact]
        public void When_PublishEventNameFinalInvalidCharacter_Then_Fail()
        {
            //// Arrange  
            //var eventname = "ABC123%%$$#";
            //var logger = Mock.Of<ILogger>();
            //Configuration.Setup(c => c.PublisherHost).Returns("localhost:8080");

            //// Action
            //var controller = new EventHubController(logger, Configuration.Object);
            //var result = controller.Publish(eventname, null) as ErrorResult;

            //Assert.NotNull(result);
            //Assert.Equal(400, result.StatusCode);

            //var error = result.Value as Error;
            //Assert.NotNull(error);
            //Assert.Equal("Invalid characters found in event name, please review it [.a-zA-Z0-9_-].", error.Message);
        }

        [Fact]
        public void When_PublishEventNameMixedInvalidCharacter_Then_Fail()
        {
            //// Arrange  
            //var eventname = "ABC123%%$$#DEF456";
            //var logger = Mock.Of<ILogger>();
            //Configuration.Setup(c => c.PublisherHost).Returns("localhost:8080");

            //var controller = new EventHubController(logger, Configuration.Object);
            //var result = controller.Publish(eventname, null) as ErrorResult;

            //Assert.NotNull(result);
            //Assert.Equal(400, result.StatusCode);

            //var error = result.Value as Error;
            //Assert.NotNull(error);
            //Assert.Equal("Invalid characters found in event name, please review it [.a-zA-Z0-9_-].", error.Message);
        }

        [Fact]
        public void When_PublishEmptyData_Then_Fail()
        {
            //// Arrange  
            //var eventname = "ABC123";
            //var logger = Mock.Of<ILogger>();
            //Configuration.Setup(c => c.PublisherHost).Returns("localhost:8080");

            //// Action
            //var controller = new EventHubController(logger, Configuration.Object);
            //var result = controller.Publish(eventname, null) as ErrorResult;

            //Assert.NotNull(result);
            //Assert.Equal(400, result.StatusCode);

            //var error = result.Value as Error;
            //Assert.NotNull(error);
            //Assert.Equal("Data value must be informed to performs this operation.", error.Message);
        }

        [Fact]
        public void When_TryPublishAllEvents_Then_Fail()
        {
            //// Arrange  
            //var eventname = "*";
            //var logger = Mock.Of<ILogger>();
            //Configuration.Setup(c => c.PublisherHost).Returns("localhost:8080");

            //// Action
            //var controller = new EventHubController(logger, Configuration.Object);
            //var result = controller.Publish(eventname, null) as ErrorResult;

            //Assert.NotNull(result);
            //Assert.Equal(400, result.StatusCode);

            //var error = result.Value as Error;
            //Assert.NotNull(error);
            //Assert.Equal("All events feature is not valid for publish.", error.Message);
        }
    }
}
